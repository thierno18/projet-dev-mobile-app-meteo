import axios from 'axios';

const url = "https://api.openweathermap.org/data/2.5";
export default axios.create({
    baseURL: url,
    timeout: 1000,
});
