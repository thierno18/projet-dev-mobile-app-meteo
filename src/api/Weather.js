import Axios from "./Axios";

const API_KEY = '0b56a513cd51e5ce828c551dd857b0ac';
const UNITS = 'metric';


/***
 * function which retrieves the weather of a city by its geographic coordinates
 * @param altitude
 * @param longitude
 * @returns {Promise<[]>}
 */

export async function getLocationCurrentWeatherByCoordinates(altitude, longitude) {
    try {

        const endpoint = `/weather?lat=${altitude}&lon=${longitude}&units=metric&appid=${API_KEY}`;
        const result = await Axios.get(endpoint);
        const data = await toLocationCurrentWeather(result.data);
       // console.log(data);
        return data;
    } catch (error) {
        console.log(`Error with function getLocationCurrentWeatherByCoordinates ${error.message}`);
        throw error;
    }
};

export async function getLocationForecast(altitude, longitude) {
    try {
        const endpoint = `/onecall?lat=${altitude}&lon=${longitude}&appid=${API_KEY}&units=${UNITS}`;
        const result = await Axios.get(endpoint);
        const data = toLocationForecast(result.data);
       // console.log(data);
        return data;
    } catch (error) {
        console.log(`Error with function getLocationWeather ${error.message}`);
        throw error;
    }
};

export async function getCurrentWeatherByCityName(name) {
    try {

        const endpoint = `/weather?q=${name}&units=${UNITS}&appid=${API_KEY}`;
        const result = await Axios.get(endpoint);
        const data = toLacationDATA(result.data)
       // console.log("getCurrentWeatherByCityName");
       // console.log(data);
        return data;
    } catch (error) {
        console.log(`Error with function getCurrentWeatherByCityID ${error.message}`);
        throw error;
    }
};

export async function getCurrentWeatherByCityID(ID) {
    try {
        const endpoint = `/weather?id=${ID}&units=${UNITS}&appid=${API_KEY}`;
        const result = await Axios.get(endpoint);
        const data = toLacationDATA(result.data)
       // console.log("getCurrentWeatherByCityID");
       // console.log(data);
        return data;
    } catch (error) {
        console.log(`Error with function getCurrentWeatherByCityID ${error.message}`);
        throw error;
    }
};



const toLocationCurrentWeather = (data) => {
    return {
        id: data.id,
        name: data.name,
        timezone: data.timezone,
        coord: {
            lat: data.coord.lat,
            lon: data.coord.lon,
        },
        weather: data.weather,
        main: data.main,
        visibility: data.visibility,
        dt: data.dt,
    };
};

const toLocationForecast = (data) => {
    return {
        hourly: data.hourly,
        daily: data.daily,
    };
};

const toLacationDATA = (data) =>{
    return{
        id: data.id,
        locationName: data.name,
        temperature: data.main.temp,
       // weatherState: data.weather[0].description,
        weatherState:"sunny",
    };
}
