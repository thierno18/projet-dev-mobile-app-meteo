const keyName = "configFile.json" // the configuration file

import {AsyncStorage} from "react-native";


async function loadConfiguration() {
    try {
        return JSON.parse(await AsyncStorage.getItem(keyName));
    } catch (error) {
        console.log(error);
    }
}

async function saveConfiguration(data) {
    try {
        await AsyncStorage.setItem(keyName, JSON.stringify(data));
    } catch (error) {
        console.log(error);
    }
}

export {loadConfiguration, saveConfiguration};