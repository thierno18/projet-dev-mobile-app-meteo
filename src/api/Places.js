const API_KEY = 'AIzaSyDeDUV39ZEdd4Na1lT8uU87LIPKgzY5Czw'; // bilal key billing activated
// const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${API_KEY}`;


// auto complete address
export async function placeAutocomplete(address) {
    try {

        const myHeaders = new Headers({'user-key': API_KEY});
        const url = `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${address}&key=${API_KEY}`;
        const response = await fetch(url, {headers: myHeaders});
        const jsonResponse = await response.json();

        let result = [];

        for (const loc of jsonResponse.predictions)
            result.push(loc.description);

        //  console.log(result);
        return result;

    } catch (error) {
        console.log(`Error with function placeAutocomplete ${error.message}`);
        throw error;
    }
};

/* return result example
    {
        "lat": 49.1205492,
        "lng": 6.160343500000001,
    }
*/
export async function getAddressCoordinates(address) {
    try {
        const myHeaders = new Headers({'user-key': API_KEY});
        const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${address}&key=${API_KEY}`;
        const response = await fetch(url, {headers: myHeaders});
        const jsonResponse = await response.json();

        return jsonResponse.results[0].geometry.location;

    } catch (error) {
        console.log(`Error with function getAddressCoordinates ${error.message}`);
        throw error;
    }
};