import React, {useState} from 'react';

import {createStackNavigator} from "@react-navigation/stack";
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {BottomNavigation, BottomNavigationTab, Icon} from '@ui-kitten/components';
import {StyleSheet, TouchableHighlight} from 'react-native';


import {SearchLocationScreen} from "../components/SearchLocationScreen";
import WeatherDetailScreen from "../components/WeatherDetailScreen";
import MyPlacesFavoriteScreen from "../components/MyPlacesFavoriteScreen";


const SearchLocationNavigator = createStackNavigator();
const WeatherDetailNavigator = createStackNavigator();
const PlacesFavoriteNavigator = createStackNavigator();
const {Navigator, Screen} = createBottomTabNavigator();



const SearchLocationScreenInstance =   () => {
    return (
        <SearchLocationNavigator.Navigator initialRouteName="SearchForLocation">
            <SearchLocationNavigator.Screen name="SearchForLocation" component={SearchLocationScreen}
                                            options={({navigation}) => ({
                                                title: 'Search Location',
                                                headerTitleStyle: {fontFamily: 'OpenSans-Regular'}
                                            })}>

            </SearchLocationNavigator.Screen>
        </SearchLocationNavigator.Navigator>
    )
};

const WeatherDetailScreenInstance  = () => {
    const [routeData, setRouteData] = useState("Metz");
    return (
        <WeatherDetailNavigator.Navigator>
            <WeatherDetailNavigator.Screen name="DetailsForLocation" component={WeatherDetailScreen}
                                            initialParams={{address: routeData}}
                                            options={({navigation, route}) => ({
                                                headerShown: false

                                            })}>

            </WeatherDetailNavigator.Screen>
        </WeatherDetailNavigator.Navigator>
    )
};


const PlacesFavoriteScreenInstance = () => {
    return (
        <PlacesFavoriteNavigator.Navigator>
            <PlacesFavoriteNavigator.Screen name="SavedLocations" component={MyPlacesFavoriteScreen}

                                            options={({navigation}) => ({
                                                title: 'Saved locations',
                                                headerTitleStyle: {fontFamily: 'OpenSans-Regular'},
                                                headerRight: () => (
                                                    <TouchableHighlight onPress={() => {
                                                        navigation.navigate("SearchLocationScreen",navigation)
                                                    }} underlayColor="#DDDDDD"
                                                                        appearance='ghost' style={styles.AddBtnStyle}>
                                                        <Icon fill="gray" style={{width: 35, height: 35}}
                                                              name="plus-circle-outline"/>
                                                    </TouchableHighlight>
                                                ),
                                            })}/>
        </PlacesFavoriteNavigator.Navigator>
    )
};


const BottomTabBar = ({navigation, state}) => (
    <BottomNavigation appearance='noIndicator'
        selectedIndex={state.index}
        onSelect={index => navigation.navigate(state.routeNames[index])}>
        <BottomNavigationTab title='Weather' icon={(props) => {
            return (<Icon {...props} name='sun-outline'/>)
        }}/>
        <BottomNavigationTab title='My Locations' icon={(props) => {
            return (<Icon {...props} name='star-outline'/>)
        }}/>
    </BottomNavigation>
);


export const AppNavigator =  () => (
    <NavigationContainer>
        <Navigator tabBar={props => <BottomTabBar {...props} />}>
            <Screen name='WeatherDetailScreen' component={WeatherDetailScreenInstance}/>
            <Screen name='PlacesFavoriteScreen' component={PlacesFavoriteScreenInstance}/>
            <Screen name='SearchLocationScreen' component={ SearchLocationScreenInstance}/>
        </Navigator>
    </NavigationContainer>
);

const styles = StyleSheet.create({

    AddBtnStyle: {

        // backgroundColor: "lightgray",
        borderWidth: 0,
        borderColor: "gray",
        marginRight: 9,
        marginTop: 3,
        width: "100%",
        height: "100%",
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center'

    },
    addBtnIconStyle: {
        fontFamily: "icomoon",
        color: "#383838",
        fontSize: 30,
        textShadowColor: 'rgba(50, 50, 50, 0.3)',
        textShadowOffset: {width: -1, height: 0},
        textShadowRadius: 6
    },

});
