
const initialState = { myPlacesID: [] }

function myPlaces(state=initialState, action){
    let nextState
    switch (action.type){
        case 'SAVE_PLACE':
            nextState = {
                ...state,
                myPlacesID: [...state.myPlacesID, action.value]
            };
            return nextState || state
        case 'UNSAVE_PLACE':
            nextState = {
                ...state,
                myPlacesID: state.myPlacesID.filter(id => id !== action.value)
            };
            return nextState || state
        default:
            return state
    };
}

export default myPlaces;
