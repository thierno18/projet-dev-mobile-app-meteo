import { createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';

import myPlacesReducer from './reducers/MyPlaces';

export default createStore(myPlacesReducer);

const configPersist = {
    key: 'root',
    storage: AsyncStorage,
};

const reducerPersist = persistReducer(configPersist, myPlacesReducer);

export const Store = createStore(reducerPersist);
export const Persistor = persistStore(Store);
