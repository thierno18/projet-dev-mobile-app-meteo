import React, {useState, Component} from 'react';
import {StyleSheet,TouchableHighlight} from "react-native";
import {Layout, Text} from "@ui-kitten/components";

import Swipeout from 'react-native-swipeout';

export class LocationListItem extends Component {


    weatherStateProps = {
        'sunny': {icon: '\u{e90e}', bgColor: '#6eb6d2', iconColor: "gold"},
        'cloudy': {icon: '\u{e906}', bgColor: '#AAAAA9', iconColor: "#F4F4F4"},
        'lightning': {icon: '\u{e909}', bgColor: '#AAAAA9', iconColor: "#F4F4F4"},
        'rain': {icon: '\u{e90a}', bgColor: '#2F718C', iconColor: "#B5DAFF"}
    };


    constructor(props) {
        super(props);
        console.log("inside contruct ");
        this.state = {
            weatherStateProp: this.weatherStateProps[props.locationData.weatherState],

        };
        this.onChange = this.onChange.bind(this);
    }



    onChange(newData) {
        //this.setState({myInput: input.target.value});
    }

    swipeoutBtns = [
        {
            text: 'Delete',
            fontFamily : "OpenSans-SemiBold",
            fontStyle:"bold",
            backgroundColor:"#cc0000",

        }
    ];

    render() {
        return (
            <Layout style={styles.container}>
                <Swipeout right={this.swipeoutBtns} style={{backgroundColor:"transparent"}}>
                    <TouchableHighlight underlayColor='rgba(192,192,192,1,0.6)' onPress={()=>{this.props.navigation.navigate('DetailsForLocation')}}>
                    <Layout style={[styles.subContainer, {backgroundColor: this.state.weatherStateProp.bgColor}]}>
                        <Text style={styles.locationNameStyle}
                              category='h3'> {this.props.locationData.locationName}</Text>

                        <Layout>
                            <Text
                                style={[styles.sunStyle, {color: this.state.weatherStateProp.iconColor}]}> {this.state.weatherStateProp.icon} </Text>
                        </Layout>

                        <Layout style={styles.containerTop}>
                            <Text style={styles.weatherStateStyle}
                                  category='h5'> {this.props.locationData.weatherState}, {this.props.locationData.temperature}°C</Text>
                        </Layout>

                    </Layout>
                    </TouchableHighlight>
                </Swipeout>
            </Layout>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginVertical: 10
    },
    subContainer: {
        paddingVertical: 8,
        paddingHorizontal: 15,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: "lightgray",
        marginLeft: 13,
        marginRight: 13,
        //backgroundColor:'rgba(85, 161, 185, 1)',


        // backgroundColor:"#2F718C",

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    containerTop: {

        paddingBottom: 8,
        backgroundColor: "transparent"
    },
    containerWeatherInfosLeft: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    containerBottom: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    locationNameStyle: {
        fontFamily: "OpenSans-Regular",
        color: "white",

        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 10,
    },

    weatherStateStyle: {
        fontFamily: "OpenSans-Regular",
        color: "white",
        textShadowColor: 'rgba(0, 0, 0, 0.55)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 5,
        backgroundColor: "transparent"
    },

    sunStyle: {
        fontFamily: 'icomoon',
        textAlign: 'right',
        fontSize: 73,

        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: 0, height: 0},
        textShadowRadius: 5,
        position: "absolute",
        marginTop: -40,

        right: -15
    }

});
