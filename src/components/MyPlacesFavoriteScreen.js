import React, { useState, useEffect } from 'react';
import {SafeAreaView, StyleSheet, FlatList} from 'react-native';
import {Button, Divider, Layout} from '@ui-kitten/components';
import { connect } from 'react-redux';
import {LocationListItem} from "./LocationListItem";
import {getCurrentWeatherByCityID} from '../api/Weather';

 const MyPlacesFavoriteScreen = ({navigation,myPlaces}) => {

    const [places, setPlaces] = useState([]);
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        refreshPlaces();
    }, [myPlaces]);

    const refreshPlaces = async () => {
        setIsRefreshing(true);
        setIsError(false);
        let locations = [];
        try {
            for (const id of myPlaces) {
                const apiSearchResult = await getCurrentWeatherByCityID(id)
                console.log("getCurrentWeatherByCityID");
                console.log(apiSearchResult);
                locations.push(apiSearchResult);
            };
            setPlaces(locations);
        } catch (error) {
            setIsError(true);
            setPlaces([]);
        }
        setIsRefreshing(false);
    };
    const navigateToLocation = () => {
        navigation.navigate('LocationScreen');
    };

    const DATA = [
        {
            id: '1',
            locationName: 'Paris',
            temperature: 8,
            weatherState: "rain"
        },
        {
            id: '2',
            locationName: 'Metz',
            temperature: 9,
            weatherState: "cloudy"
        },
        {
            id: '3',
            locationName: 'Nador',
            temperature: 14,
            weatherState: "sunny"
        },
    ];

    return (
        <Layout style={styles.container}>
            <FlatList style={styles.subContainer}
                      data={places}
                      keyExtractor={item => item.id}
                      renderItem={({item}) => (
                          <LocationListItem locationData={item}/>
                      )}
            />
        </Layout>
    );
};

const mapStateToProps = (state) => {
    return {
        myPlaces: state.myPlacesID
    }
}
export default  connect(mapStateToProps)(MyPlacesFavoriteScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 8,
    },
    subContainer: {
        flex: 1,
    },
});
