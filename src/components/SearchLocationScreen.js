import React, {useState,} from 'react';
import {TouchableOpacity, FlatList} from 'react-native';
import {Layout, Text, Input, Icon} from '@ui-kitten/components';
import {StyleSheet} from 'react-native';
import {placeAutocomplete} from '../api/Places';
import {connect} from "react-redux";
import { Keyboard } from 'react-native';
export const SearchLocationScreen = ({navigation}) => {

    navigation.setOptions({
        tabBarVisible: false
    });

    let searchInputRef = React.createRef();

    const [isError, setIsError] = useState(false);
    const [autoCompletedAddress, setAutoCompletedAddress] = useState([]);


    const navigateTo = async (address) => {
        Keyboard.dismiss();
        searchInputRef.current.clear();
        navigation.navigate('WeatherDetailScreen',{address});
    };


    async function searchAddress(address) {

        if (address != '') {
            try {
                const res = await placeAutocomplete(address);
                setAutoCompletedAddress(res);

            } catch (error) {
                setIsError(true);
                console.log(error);
            }
        } else
            setAutoCompletedAddress([]);
    }




    const searchIcon = (props) => (
        <Icon  {...props} name={'search'} onPress={() => {
            searchInputRef.current.focus();
        }}/>
    );


    return (

        <Layout style={styles.container}>

            <Layout style={styles.textInputContainer}>
                <Input textStyle={styles.textInputStyle} id='searchTxt' ref={searchInputRef} size='large'
                       placeholder='Input city name or  address'
                       accessoryLeft={searchIcon}
                       onChangeText={(address) => searchAddress(address)}/>
            </Layout>


            <Layout style={styles.listContainer}>

                <FlatList
                    keyboardShouldPersistTaps={'handled'}
                    data={autoCompletedAddress}
                    renderItem={({item}) =>
                        <TouchableOpacity onPress={() =>navigateTo(item)}>

                            <Text style={styles.textListItemStyle}>{item}</Text>
                        </TouchableOpacity>
                    }
                />
            </Layout>
        </Layout>
    );
};

const mapStateToProps = (state) => {
    return {
        myPlaces: state.myPlacesID
    }
}
connect(mapStateToProps)(SearchLocationScreen);

const styles = StyleSheet.create({
    container: {
        backgroundColor: "black"
    },
    textInputContainer: {},
    topContainer: {
        backgroundColor: "black",

        justifyContent: 'center',
    },

    textInputStyle: {
        fontFamily: "OpenSans-SemiBold",
        fontSize: 15,
    },

    listContainer: {

        //flex: 1,
        paddingTop: 0,
        height: "100%",
        //   flexDirection: 'row'
    },

    textListItemStyle: {
        padding: 11,
        fontSize: 16,
        minHeight: 64,
        borderBottomColor: "#aaaaaa",
        borderBottomWidth: 0.3,
        fontFamily: 'OpenSans-SemiBold',
        textAlignVertical: 'center'


    },


});


