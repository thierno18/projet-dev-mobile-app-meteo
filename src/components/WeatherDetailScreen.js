import React, {useState, useEffect, useRef} from 'react';
import {StyleSheet, Image, ScrollView, ActivityIndicator, Dimensions, View} from 'react-native';
import {Layout, Text, List, Icon} from '@ui-kitten/components';
import {format} from "date-fns";
import { connect } from 'react-redux';
import {getAddressCoordinates} from "../api/Places";
import {getLocationCurrentWeatherByCoordinates, getLocationForecast} from "../api/Weather";
import {LineChart} from "react-native-chart-kit";

const WeatherDetailScreen = ({route, navigation, myPlaces, dispatch}) => {
    const [hourly, setHourly] = useState(null);
    const [daily, setDaily] = useState(null);


    const [currentWeather, setCurrentWeather] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus',()=>{
            requestCurrentWeather();
        })

        return unsubscribe;
    }, [navigation]);

    const requestCurrentWeather = async () => {
        try {
            const coordination =  await getAddressCoordinates(route.params.address);
            const w = await getLocationCurrentWeatherByCoordinates(coordination.lat, coordination.lng);
            setCurrentWeather(w);
            const action = { type: 'SAVE_PLACE', value: currentWeather.id};
            dispatch(action);
            const f = await getLocationForecast(coordination.lat, coordination.lng);
            setDaily(f.daily);
            setHourly(f.hourly);
            setIsLoading(false);
        } catch (error) {
            setIsError(true);
        }
    };


    const renderItemHourly = ({item}) => {
        const dt = new Date(item.dt * 1000);
        return (
            <Layout style={styles.hourly}>
                <Text style={styles.theTextStyle} category='h6'>{dt.getHours()}h</Text>

                <Image style={[styles.icon, {marginTop: 5,}]}
                       source={{uri: 'http://openweathermap.org/img/wn/' + item.weather[0].icon + '@2x.png'}}/>


                <Text style={styles.theTextStyle} category='h6'>{Math.round(item.temp)}°</Text>
            </Layout>
        );
    };

    const renderItemDaily = ({item}) => {
        const dt = new Date(item.dt * 1000);
        return (
            <Layout style={styles.daily}>
                <Layout style={styles.dayName}>
                    <Text style={styles.theTextStyle} category='h6'>{format(dt, "EEEE")}</Text>
                    <Image style={styles.icon}
                           source={{uri: 'http://openweathermap.org/img/wn/' + item.weather[0].icon + '@2x.png'}}/>
                </Layout>
                <Layout style={styles.dayTemp}>
                    <Text style={styles.theTextStyle} category='h6'>{Math.round(item.temp.min)}°</Text>
                    <Text style={styles.theTextStyle} category='h6'>{Math.round(item.temp.max)}°</Text>
                </Layout>
            </Layout>
        );
    };


    const tempValuesCountToSelect = 6;

    function loadTemperaturesLabels() {
        let arr = [];

        for (let i = 0; i < tempValuesCountToSelect; i++) {
            const dt = new Date(hourly[i].dt * 1000);
            arr.push(dt.getHours() + "h");
        }
        return arr;
    }

    function loadTemperaturesValues() {
        let arr = [];

        for (let i = 0; i < tempValuesCountToSelect; i++) {
            console.log(Math.round(hourly[i].temp));
            arr.push(Math.round(hourly[i].temp));
        }
        return arr;
    }
    return (
        <Layout style={styles.container}>
            {
                (isLoading ?
                    (<Layout style={styles.containerLoading}>
                        <ActivityIndicator size="large"/>
                    </Layout>) : (
                        <ScrollView style={styles.subContainer}>
                            <Layout style={styles.containerTop}>
                                <Layout>
                                    <Layout style={{alignItems: 'center'}}>
                                        <Text category='h3' style={styles.locationName}>{currentWeather.name}</Text>
                                        <Text category='h6'
                                              style={styles.locationName}>{currentWeather.weather[0].description}</Text>
                                    </Layout>
                                    <Layout style={styles.weather}>
                                        <Image  style={[styles.icon, {marginTop: -2,}]}
                                               source={{uri: 'http://openweathermap.org/img/w/' + currentWeather.weather[0].icon + '.png'}}/>
                                        <Text category='h2'
                                              style={styles.weatherState}> {Math.round(currentWeather.main.temp)}°C</Text>
                                    </Layout>
                                </Layout>
                            </Layout>
                            <Layout style={styles.containerBottom}>

                                <Layout style={styles.containerHourly}>
                                    <Text category='h5' style={styles.title}>Hourly Forecast</Text>
                                    <List style={styles.hourlyListStyle} horizontal={true}
                                          showsHorizontalScrollIndicator={false}
                                          data={hourly}
                                          renderItem={renderItemHourly}
                                    />
                                </Layout>


                                <Layout  horizontal={true} style={styles.containerHourly}>

                                    <Text category='h5' style={styles.title}>Hourly Chart</Text>

                                    <Layout style={styles.chartContainer}>
                                    <LineChart
                                        data={{
                                            labels: loadTemperaturesLabels(),
                                            datasets: [
                                                {
                                                    data: loadTemperaturesValues(),
                                                    color: (opacity = 1) => `rgba(12, 33, 244, ${opacity})` // optional
                                                }
                                            ]


                                        }}
                                        width={Dimensions.get("window").width - 40} // from react-native
                                        height={220}
                                        // yAxisLabel=""
                                         yAxisSuffix="°C"
                                        yAxisInterval={1} // optional, defaults to 1
                                        chartConfig={{
                                            backgroundColor: "white",
                                            backgroundGradientFrom: "white",
                                            backgroundGradientTo: "white",
                                            decimalPlaces: 0, // optional, defaults to 2dp
                                            color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
                                            // labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                            style: {
                                                borderRadius: 16,

                                            },
                                            propsForDots: {
                                                r: "6",
                                                strokeWidth: "2",
                                                stroke: "#ffa726"
                                            }
                                        }}
                                        bezier
                                        style={{
                                            marginVertical: 8,
                                            borderRadius: 16
                                        }}
                                    />

                                </Layout>
                                </Layout>

                                <Layout style={styles.containerDaily}>

                                    <View >
                                        <Text category='h5' style={styles.title}>Daily Forecast</Text>

                                    <Text  style={{textAlign: 'left', marginLeft:-20 }}>Min</Text>

                                    </View>

                                    <List style={{height: '100%'}} showsVerticalScrollIndicator={false}
                                          scrollEnabled={false}

                                          data={daily}
                                          renderItem={renderItemDaily}
                                    />
                                </Layout>

                            </Layout>
                        </ScrollView>))
            }
        </Layout>
    );
};
const mapStateToProps = (state) => {
    return {
        myPlaces: state.myPlacesID
    }
}
export default  connect(mapStateToProps)(WeatherDetailScreen);


const styles = StyleSheet.create({

    theTextStyle: {
        fontFamily: "OpenSans-SemiBold",
        fontSize: 17,
        textAlign: 'center'
    },

    hourlyListStyle: {
        marginLeft: 7,
        marginBottom: 10,
    },

    container: {
        flex: 1,
        fontFamily: "OpenSans-SemiBold"
    },
    subContainer: {
        flex: 1,
        paddingHorizontal: 16,
        paddingTop: 20,
    },
    containerTop: {
        flex: 1,
        paddingTop: 20,
    },
    locationName: {
        fontFamily: "OpenSans-SemiBold",
        color: "black",
        textShadowColor: 'rgba(233, 33, 73, 0.25)',
        textShadowOffset: {width: -1, height: 0},
        textShadowRadius: 5,
        marginTop: 20,
    },
    weather: {
        marginVertical: 16,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    weatherState: {
        fontFamily: "OpenSans-SemiBold",
        color: "black",
        textShadowColor: "rgba(0,0,0,0.5)",
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 15,
        alignItems: 'center',


        // backgroundColor: "black"
    },


    containerBottom: {
        flex: 2,
        //


    },
    containerHourly: {
        flex: 1,
        marginBottom: 26,
        paddingBottom: 9,
        borderWidth: 1,
        borderColor: "gray",


        fontFamily: "OpenSans-SemiBold",

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    hourly: {
        flex: 1,
        justifyContent: 'space-between',

    },
    containerGraphical: {
        flex: 1,
        marginBottom: 16,
        borderWidth: 2,

    },
    containerDaily: {
        flex: 2,
        borderWidth: 1,
        borderColor: "gray",
        height: '100%',
        marginBottom: 30,
        fontFamily: "OpenSans-SemiBold",

        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,


    },
    daily: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',


    },
    dayName: {
        flex: 2,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginLeft: 9,

        fontFamily: "OpenSans-regular"
    },
    dayTemp: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 60,
        marginRight: 20,
        paddingBottom : 10,


    },
    title: {
        marginTop: 5,
        marginLeft: 5,
        marginBottom: 25,
        borderBottomWidth: 0,
        borderColor: "gray",
        fontFamily: "OpenSans-SemiBold",
        // fontWeight: "bold"
    },
    icon: {
        width: 52,
        height: 52,
        marginTop: -10,


        resizeMode: 'cover',
        borderBottomLeftRadius: 16,
        borderBottomRightRadius: 16,
        shadowColor: '#202020',
        shadowOffset: {width: 0, height: 0},
        shadowRadius: 5,
        overflow: "visible"

    },
    containerLoading: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    chartContainer:{

    }
});
