import {StatusBar} from 'expo-status-bar';
import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {useState} from 'react';

import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import {Store, Persistor} from './src/store/config';

import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry, Layout} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {default as theme} from './theme.json';

import {AppNavigator} from "./src/navigation/Navigator";
import * as Font from "expo-font";
import {loadConfiguration, saveConfiguration} from './src/api/configFileManagement'
import {LogBox} from 'react-native';
import {getAddressCoordinates} from "./src/api/Places";
import {getLocationCurrentWeatherByCoordinates, getLocationForecast} from "./src/api/Weather";

export default function App() {
    //  LogBox.ignoreLogs(['Warning: componentWillMount has been renamed, and is not recommended for use. See https://fb.me/react-unsafe-component-lifecycles for details.']);
    //  LogBox.ignoreAllLogs(true);
    const [isFontLoaded, setIsFontLoaded] = useState(false);

    async function componentDidMount() {

        let readConfig = await loadConfiguration();


        if (readConfig) {
            global.isConfigSet = true;
            global.config = readConfig;
        } else


            global.isConfigSet = true;

        //  await saveConfiguration(global.config);


        await Font.loadAsync({
            'OpenSans-Regular': require('./assets/fonts/OpenSans-Regular.ttf'),
            'OpenSans-SemiBold': require('./assets/fonts/OpenSans-SemiBold.ttf'),
            'OpenSans-Italic': require('./assets/fonts/OpenSans-Italic.ttf'),
            'icomoon': require('./assets/fonts/icomoon.ttf')
        });
        setIsFontLoaded(true);
    }

    componentDidMount();

    if (isFontLoaded) {
        return (
            <>
                <Provider store={Store}>
                    <PersistGate loading={null} persistor={Persistor}>
                        <IconRegistry icons={EvaIconsPack}/>
                        <ApplicationProvider {...eva} theme={{...eva.light, ...theme}}>
                            <Layout style={styles.container}>
                                <AppNavigator/>
                                <StatusBar style="auto"/>
                            </Layout>
                        </ApplicationProvider>
                    </PersistGate>
                </Provider>
            </>
        );
    } else {
        return (
            <>
                <Text> </Text>
            </>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
        marginTop: 16

    },
});
